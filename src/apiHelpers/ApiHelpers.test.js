import axios from 'axios';
import MockAdapter from 'axios-mock-adapter'
import { AddTodo, FetchTodos } from './ApiHelpers'

describe('FetchTodos should recieve an array of todos', () => {
    let mock

    beforeAll(() => {
        mock = new MockAdapter(axios)
    })

    afterEach(() => {
        mock.reset()
    })

    describe('When API call is succesfull', () => {
        it("should return the array of todos", async () => {

            // given the array
            const responseArray = [
                { todoName: "todo1", id: "asdas-xcvsdv-asdas" },
                { todoName: "todo2", id: "asdsfsdfas-xcvsdxcbxv-asdzxczxas" },
                { todoName: "todo3", id: "asdjrrgeas-xcvsdbxcv-asadasddas" }
            ]
            mock.onGet(`${process.env.REACT_APP_BACKEND_URL}/todos`).reply(200, responseArray)

            const todos = await FetchTodos(`${process.env.REACT_APP_BACKEND_URL}/todos`)

            expect(mock.history.get[0].url).toEqual(`${process.env.REACT_APP_BACKEND_URL}/todos`)
            expect(Array.isArray(todos)).toBe(true)

            expect(todos[0].todoName).toEqual(responseArray[0].todoName)

            for (const todo of todos) {
                expect(todo).toHaveProperty('id')
                expect(todo).toHaveProperty('todoName')
            }
        })
    })
})

describe('AddTodo should add a new todo', () => {
    let mock

    beforeAll(() => {
        mock = new MockAdapter(axios)
    })

    afterEach(() => {
        mock.reset()
    })

    describe('When API call is succesfull', () => {
        it("should be able to add a new todo to the backend and the todo must be returned back", async () => {
            //given the new todo to add
            const responseTodo = {
                todoName: "Test todo",
                id: "adasdas-asdasdzxczx-zxczxcsc-asdas"
            }

            mock.onPost(`${process.env.REACT_APP_BACKEND_URL}/addTodo`).reply(201, responseTodo)

            const result = await AddTodo(`${process.env.REACT_APP_BACKEND_URL}/addTodo`, responseTodo)

            expect(mock.history.post[0].url).toEqual(`${process.env.REACT_APP_BACKEND_URL}/addTodo`)
            expect(result.todoName).toEqual(responseTodo.todoName)
            expect(result.id).toEqual(responseTodo.id)
        })
    })
})
