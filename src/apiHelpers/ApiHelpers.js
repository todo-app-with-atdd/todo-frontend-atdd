import axios from "axios";

/**
 * Fetches an empty array if there is no todo in the backend, else array of todos
 *
 * @export
 * @param {*} URL
 * @return {*} 
 */
export async function FetchTodos(URL) {
    return (await axios.get(URL)).data
}


/**
 * Sends a POST req to the api to add todo with a new todo object
 *
 * @export
 * @param {*} URL
 * @param {*} newTodo
 * @return {*} 
 */
export async function AddTodo(URL, newTodo) {
    return (await axios.post(URL, newTodo)).data
}