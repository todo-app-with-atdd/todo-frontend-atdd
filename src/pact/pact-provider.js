const { Pact } = require('@pact-foundation/pact');
const path = require('path');

global.port = 3007

// Pact Provider
export default new Pact({
    consumer: 'Todo App Frontend',
    provider: 'Todo App Backend',
    port: global.port,
    log: path.resolve(process.cwd(), 'logs', 'pact.log'),
    dir: path.resolve(process.cwd(), 'pacts'),
    logLevel: 'INFO',
    cors: true
})