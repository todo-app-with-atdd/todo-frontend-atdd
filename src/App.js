import './App.css';
import Heading from './components/Heading/Heading'
import InputBox from './components/InputBox/InputBox'
import AddButton from './components/AddButton/AddButton'
import TodoList from './components/TodoList/TodoList'
import StaticData from './data/static-data';
import { useEffect, useState } from 'react';
import { AddTodo, FetchTodos } from './apiHelpers/ApiHelpers'
import { AddNewTodoToList } from './helpers/HelperFunctions';


function App() {

  const [input, setInput] = useState("")
  const [todos, setTodos] = useState([])
  const [isDisabled, setIsDisabled] = useState(false)

  useEffect(() => {
    (async () => {
      setTodos(await FetchTodos(`${process.env.REACT_APP_BACKEND_URL}/todos`))
    })()
  }, [])

  const handleClick = async () => {
    if (input.length === 0) {
      alert("You should enter a todo name to the box!")
      return
    }
    setIsDisabled(true)

    const newTodoAdded = await AddTodo(`${process.env.REACT_APP_BACKEND_URL}/addTodo`, { todoName: input })
    const newTodoList = AddNewTodoToList(todos, newTodoAdded)

    setIsDisabled(false)

    setTodos(newTodoList)
    setInput("")
  }

  return (
    <div className="App">
      <Heading text={StaticData.ApplicationTitle} />

      <div className="AddTodoContainer" >
        <InputBox onChangeMethod={(e) => setInput(e.target.value)} statefulValue={input} placeholderText={StaticData.TextBoxPlaceHolder} />
        <AddButton statefulDisabled={isDisabled} statefulOnClick={handleClick} label={StaticData.AddButtonLabelText} />
      </div>

      <div data-testid="todoList" className="todoList">
        <TodoList todos={todos} />
      </div>

    </div>
  );
}

export default App;