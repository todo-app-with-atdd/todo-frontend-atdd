import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import StaticData from '../../data/static-data'
import Heading from './Heading'

describe("Heading renders correctly", () => {
    const ApplicationHeading = StaticData.ApplicationTitle
    render(<Heading text={ApplicationHeading} />)

    // Component test for the Heading
    it(`Should render a heading with the text: ${ApplicationHeading}`, () => {
        expect(screen.getByText(ApplicationHeading)).toBeInTheDocument()
    })
})