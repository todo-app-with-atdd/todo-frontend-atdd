import React from "react";

function AddButton({ label, statefulOnClick, statefulDisabled }) {
    return (
        <button disabled={statefulDisabled} onClick={statefulOnClick} className="addButton" >{label}</button>
    )
}

export default AddButton