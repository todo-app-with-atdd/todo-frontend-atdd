import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import StaticData from '../../data/static-data'
import AddButton from './AddButton'

describe("Add Button renders correctly", () => {
    const buttonLabel = StaticData.AddButtonLabelText
    it(`Should render add button with the text: ${buttonLabel}`, () => {
        render(<AddButton label={buttonLabel} />)
        expect(screen.getByText(buttonLabel)).toBeInTheDocument()
    })
})