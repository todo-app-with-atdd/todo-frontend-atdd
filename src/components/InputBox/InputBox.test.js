import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import StaticData from '../../data/static-data'
import InputBox from './InputBox.js'

describe('InputBox renders correctly', () => {
    const inputPlaceHolder = StaticData.TextBoxPlaceHolder
    it(`Should render an input box with the placeholder:  `, () => {
        render(<InputBox placeholderText={inputPlaceHolder} />)
        expect(screen.getByPlaceholderText(inputPlaceHolder)).toBeInTheDocument()
    })
})
