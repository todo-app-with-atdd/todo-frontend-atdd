import React from "react";

function InputBox({ placeholderText, onChangeMethod, statefulValue }) {
    return (
        <input data-testid="addTodoBox" onChange={onChangeMethod} value={statefulValue} type="text" className="addTodoBox" placeholder={placeholderText} />
    )
}

export default InputBox