import React from "react";
import TodoItem from "../TodoItem/TodoItem";

function TodoList({ todos }) {
    return !todos || todos.length === 0
        ?
        <p>No Todo Yet!</p>
        :
        (todos.map(todo => (
            <TodoItem key={todo.id} todoName={todo.todoName} />
        )))
}

export default TodoList