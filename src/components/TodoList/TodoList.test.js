import { cleanup, render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import StaticData from '../../data/static-data'
import TodoList from './TodoList'

describe('TodoList renders correctly', () => {
    const emptyTodoListText = StaticData.EmptyTodoListText
    const todos = [{ todoName: "todo1", id: "sfasd" }, { todoName: "todo2", id: "asfasfdzc" }, { todoName: "todo3", id: "sdfnsdfajn" }]

    // Cleanup the rendered elements after each test closure
    afterEach(() => {
        cleanup()
    })

    it(`Should render a text saying '${emptyTodoListText}' when the list has an empty array of todo items`, () => {
        render(<TodoList todos={[]} />)
        expect(screen.getByText(emptyTodoListText)).toBeInTheDocument()
    })


    it(`Should render all given todos`, () => {
        render(<TodoList todos={todos} />)
        for (const todo of todos) {
            expect(screen.getByText(todo.todoName)).toBeInTheDocument()
        }
    })

})

