import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'

import TodoItem from './TodoItem'

describe('TodoItem Renders correctly', () => {
    const todoName = "TEST_TODO"
    it(`Should render TodoItem with the given todo name: ${todoName}`, () => {
        render(<TodoItem todoName={todoName} />)
        expect(screen.getByText(todoName)).toBeInTheDocument()
    })
})
