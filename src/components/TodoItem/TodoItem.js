import React from "react";

function TodoItem({ todoName }) {

    return (
        <div data-testid="todoItem" className="todoItem">
            <p className="todoText" >{todoName}</p>
        </div>
    )
}

export default TodoItem