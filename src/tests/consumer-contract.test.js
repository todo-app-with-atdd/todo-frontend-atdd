import { eachLike, like } from '@pact-foundation/pact/src/dsl/matchers'
import { AddTodo, FetchTodos } from '../apiHelpers/ApiHelpers'
import provider from '../pact/pact-provider'

describe('Backend Mock Service', () => {
    const exampleTodo = {
        todoName: "Name of the todo"
    }

    describe('When a request is made to fetch all todos', () => {
        beforeAll(async () => {
            await provider.setup()

            // GET /todos -> mocking to return a list of todos
            await provider.addInteraction({
                state: "There are todos",
                uponReceiving: "a GET request for todos",
                withRequest: {
                    path: '/todos',
                    method: "GET"
                },
                willRespondWith: {
                    body: eachLike({ id: "b2aee263-060e-4ab6-91eb-79e59d9e7dd3", todoName: "Name of the todo" }),
                    status: 200,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                    }
                }
            })

            await provider.addInteraction({
                state: "todo can be added",
                uponReceiving: "a POST request to add a new todo",
                withRequest: {
                    path: '/addTodo',
                    method: "POST",
                    body: like({ todoName: "Name of the todo" }),
                    headers: { "Content-Type": "application/json" }
                },
                willRespondWith: {
                    status: 201,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    body: like({ id: "b2aee263-060e-4ab6-91eb-79e59d9e7dd3", todoName: "Name of the todo" })
                }
            })

            await provider.addInteraction({
                state: "can be added",
                uponReceiving: "a POST request trying to add a new todo",
                withRequest: {
                    path: '/addTodo',
                    method: "POST",
                    body: like({}),
                    headers: { "Content-Type": "application/json" }
                },
                willRespondWith: {
                    status: 400,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    body: {
                        message: "request body must contain todoName, but todoName is missing!"
                    }
                }
            })

        })

        it("FetchTodos Should receive the list of todos", async () => {
            const todos = await FetchTodos(`http://localhost:${global.port}/todos`)
            expect(Array.isArray(todos)).toBe(true)
            expect(todos[0].todoName).toBe(exampleTodo.todoName)
        })

        it("AddTodo should be adding a new todo via api", async () => {
            const response = await AddTodo(`http://localhost:${global.port}/addTodo`, exampleTodo)
            expect(response.todoName).toBe(exampleTodo.todoName)
        })

        it("AddTodo should receive a validation error message with 400 status when todoName is not passed to the request body", async () => {
            try {
                const response = await AddTodo(`http://localhost:${global.port}/addTodo`, {})
            } catch (e) {
                expect(e.response.status).toEqual(400)
                expect(e.response.data.message).toBe("request body must contain todoName, but todoName is missing!")
            }

        })

        afterAll(async () => {
            await provider.verify()
            await provider.finalize()
        })

    })
})
