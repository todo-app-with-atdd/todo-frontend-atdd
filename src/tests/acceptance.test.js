import puppeteer from "puppeteer";
import { v4 as uuidv4 } from 'uuid';

const frontendUrl = process.env.REACT_APP_FRONTEND_URL
let browser, page

beforeAll(async () => {
    browser = await puppeteer.launch({ ignoreDefaultArgs: ['--disable-extensions'] })
    page = await browser.newPage()
})

// First Acceptance Test
describe(
    `   
    Given Todo list, 
    when I write a new todo in text box and click to add button, 
    then I should see the new todo in Todo list
    `,
    () => {
        const newTodo = uuidv4()
        beforeAll(async () => {
            await page.goto(frontendUrl)
        })

        it("GIVEN Todo list", async () => {
            expect(await page.$('.todoList') !== null).toBeTruthy()
        })

        it("WHEN I write a new todo in the text box and click to add button", async () => {
            await page.type('.addTodoBox', newTodo)
            const valueOfInput = await page.$eval(".addTodoBox", (input) => {
                return input.getAttribute("value")
            })
            expect(valueOfInput).toBe(newTodo)

            await page.click('button.addButton')
        })

        it("THEN I should see the new todo in the list", async () => {
            await page.waitForSelector('.todoList .todoItem')

            while (true) {
                const todos = await page.evaluate(() => {
                    return [...document.querySelectorAll('.todoList .todoItem .todoText')].map(el => el.innerText)
                })

                if (todos.length >= 1 && todos.includes(newTodo)) {
                    expect(todos.length).toBeGreaterThanOrEqual(1)
                    expect(todos).toContain(newTodo)
                    return
                }
            }
        })
    })

afterAll(() => {
    browser.close()
})