import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import StaticData from '../data/static-data'
import App from '../App'


describe('Application renders correctly', () => {
    beforeEach(() => {
        render(<App />)
    })

    it(`Should render a heading with the text: ${StaticData.ApplicationTitle}`, () => {
        expect(screen.getByText(StaticData.ApplicationTitle)).toBeInTheDocument()
    })

    it(`Should render an input box with the placeholder: ${StaticData.TextBoxPlaceHolder}`, () => {
        expect(screen.getByPlaceholderText(StaticData.TextBoxPlaceHolder)).toBeInTheDocument()
    })

    it(`Should render a button with the text: ${StaticData.AddButtonLabelText}`, () => {
        expect(screen.getByText(StaticData.AddButtonLabelText)).toBeInTheDocument()
    })

    it(`Should render the todo list with todos if there are, otherwise with a text: ${StaticData.EmptyTodoListText}`, async () => {
        try {
            // if list is empty
            await screen.findByText(StaticData.EmptyTodoListText)
            expect(screen.getByText(StaticData.EmptyTodoListText))
        } catch (e) {
            // if list is not empty
            expect(screen.getAllByTestId('todoItem').forEach(todo => expect(todo.className).toEqual("todoItem")))
        }
    })

})
