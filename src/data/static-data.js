const staticData = {
    ApplicationTitle: "TODO APP WITH A-TDD",
    TextBoxPlaceHolder: "New todo name:",
    AddButtonLabelText: "ADD",
    EmptyTodoListText: "No Todo Yet!"
}

export default staticData