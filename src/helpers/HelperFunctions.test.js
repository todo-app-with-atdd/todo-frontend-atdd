import { AddNewTodoToList } from './HelperFunctions'

describe('Helper Functions run correctly', () => {

    it("AddNewTodoToList should add a new todo to a list and returns a new list with all todos", () => {
        const todos = [{ todoName: "todo1", id: "zxczxc" }, { todoName: "todo1", id: "aDAdxzcv" }]
        const newTodo = { todoName: "New Todo to add", id: "fadshkfasb" }

        // function must return an array containing the new and previous todos
        const newTodosIds = (AddNewTodoToList(todos, newTodo)).map(todo => todo.id)

        expect(newTodosIds.includes(newTodo.id)).toBeTruthy()

        for (const todo of todos) {
            expect(newTodosIds.includes(todo.id)).toBeTruthy()
        }
    })

})
