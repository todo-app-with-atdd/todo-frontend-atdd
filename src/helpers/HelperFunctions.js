/**
 * Returns a new Array containing todos and the newTodo
 *
 * @param {*} todos
 * @param {*} newTodo
 * @return {*} 
 */
export function AddNewTodoToList(todos, newTodo) {
    return todos.concat([newTodo])
}