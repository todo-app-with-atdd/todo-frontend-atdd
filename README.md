# ⭐ Todo Application designed with Acceptance Test Driven Design Methodology
* Demo: http://34.116.151.253/
* The application is developed with ReactJS as a UI of the todo application
* Current ability for this app is only "Adding a new todo" to the system

# 🚀 Tech Stack
 * React JS
 * Axios JS
 * Pact JS


# 💿 How to run local
```
# Clone this repository
https://gitlab.com/todo-app-with-atdd/todo-frontend-atdd.git
# Go into the repository
cd todo-frontend-atdd
# Install the dependencies
$ npm install
# You need to modify example.env file with your own configs
# Start to see the application in the localhost:3000
$ npm run start:DEV
```

# 💿 How to test local
```
# Run all the tests and see the results
$ npm run test
```

# 🧠 Architectural Decisions
* Application is mainly located on App.js
* Whole application is tested (component tests) inside app.test.js
* All components are developed under Components folder as single units (so everythin is independent of each other)
* All components are tested (component tests) with unit tests inside their folders
* Acceptance criteria (GIVEN todo list, WHEN I write "some text" to <text-box> anc click to <addButton>, THEN I should see the newly added todo in the list) is tested inside acceptance.test.js with puppeteer.js
* Api helper functions and normal helper functions are also tested inside their folders
* CDC test is developed  under consumer-contract.test.js with pactjs for two API operations: FetchTodos & AddTodo
* For all the development processes, Test Driven Design methodology is followed


# ✈️ Deployment (Docker, Google Kubernetes)
* gcloud cli should be installed
* gcloud login should be made (you need an account for Google Cloud Platform)
* gcloud project should be created as React-Todo-Frontend in my case
* Set the project as default in your local: gcloud config set react-todo-frontend (in my case)
* Dockerize the application (check Dockerfile)
* Build the docker image with the command: docker build -t react-todo .
* Configure docker for gcloud: gcloud auth configure-docker
* Tag the image: docker tag react-todo gcr.io/react-todo-frontend/react-todo 
* Push the docker image to Google Container Registry: docker push gcr.io/react-todo-frontend/react-todo 
* Create Google Kubernetes Engine cluster: gcloud container clusters create react-todo
* Configure kubectl: "_use the connect command line command from the cluster you create_"
* You should have kubectl installed for above step
* Deploy kubernetes object to GKE: kubectl create -f manifest.yml (check the manifest.yml)
* See the deployed image url etc: kubectl get svc
* Deployment is done for the UI 

